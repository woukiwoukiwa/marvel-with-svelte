// ./stores/characters.ts
import { Writable, writable } from "svelte/store";
import axios from "axios";

const API_URL = "http://localhost:3000/api/characters";

export function fetchCharacters(): [
  Writable<boolean>,
  Writable<boolean>,
  Writable<[]>
] {
  const loading = writable(false);
  const error = writable(false);
  const data = writable([]);

  async function get({ limit, offset }: { limit: number; offset: number }) {
    loading.set(true);
    error.set(false);
    try {
      const response = await axios.get(API_URL, {
        params: { limit, offset },
      });
      data.set(await response.data.characters);
    } catch (e) {
      error.set(e);
    }
    loading.set(false);
  }

  get({ limit: 20, offset: 0 });

  return [data, loading, error, get];
}
